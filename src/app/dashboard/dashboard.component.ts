import {Component, OnInit, ViewChild} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

	@ViewChild('sidenav', {static: true}) sidenav: MatSidenav;

	constructor(
		public translate: TranslateService,
		private router: Router,
	) {
		translate.addLangs(['en', 'ja']);

		// Default fallback language to EN
		translate.setDefaultLang('en');

		// Get browser language
		const browserLang = translate.getBrowserLang();
		translate.use(browserLang.match(/en|ja/) ? browserLang : 'en');
	}

	toggleSideNav(event) {
		if (event) {
			this.sidenav.open();
		} else {
			this.sidenav.close();
		}
	}

	items(): void {
		this.sidenav.close();
		this.router.navigateByUrl('/items');
	}

	franchises(): void {
		this.sidenav.close();
		this.router.navigateByUrl('/franchises');
	}

	universes(): void {
		this.sidenav.close();
		this.router.navigateByUrl('/universes');
	}

	import(): void {
		this.sidenav.close();
		this.router.navigateByUrl('/import');
	}

	ngOnInit() {
	}

}
