import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {filter, map} from 'rxjs/operators';
import {FranchiseService} from '../../shared/service/franchise.service';
import {FranchiseMeta} from '../../shared/model/franchise-meta';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
	providers: [FranchiseService]
})
export class HeaderComponent implements OnInit {

	header: string;
	subHeader: string;

	@Output()
	openSideNav = new EventEmitter<boolean>();

	constructor(
		public translate: TranslateService,
		private router: Router,
		private franchiseService: FranchiseService
	) {}

	ngOnInit() {
		this.buildHeaderTitle(this.router.url.substring(1));
		this.router.events.pipe(
			filter(event => event instanceof NavigationStart)
		).subscribe(
			(event: NavigationStart) => {
				this.buildHeaderTitle(event.url.substring(1));
			}
		);
		this.translate.onLangChange.subscribe(
			() => {
				this.buildHeaderTitle(this.router.url.substring(1));
			}
		);
	}

	buildHeaderTitle(route: string) {
		if (route.includes('/')) {

			const split = route.split('/');
			this.header = 'HEADER.TITLE.' + split[0].toUpperCase();

			// Get franchise title
			this.franchiseService.getFranchiseMetas().pipe(
				map(f => f.find(i => i.id === split[1]))
			).subscribe(
				(fm: FranchiseMeta) => {
					this.subHeader = fm.title[this.translate.currentLang];
				}
			);

		} else {
			this.header = 'HEADER.TITLE.' + route.toUpperCase();
			this.subHeader = undefined;
		}
	}

	toggleSideNav() {
		this.openSideNav.emit(true);
	}

	headerNavigate() {
		const route = this.router.url.substring(0, this.router.url.lastIndexOf('/'));
		this.router.navigateByUrl(route);
	}
}
