import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {Item} from '../../shared/model/item';
import {ItemService} from '../../shared/service/item.service';
import {UtilService} from '../../shared/service/util.service';
import {TranslateService} from '@ngx-translate/core';
import {Constants} from '../../shared/constants';
import {Franchise} from '../../shared/model/franchise';

@Component({
	selector: 'app-item',
	templateUrl: './item.component.html',
	styleUrls: ['./item.component.scss'],
	providers: [ItemService]
})
export class ItemComponent implements OnInit {

	dataSource: MatTableDataSource<Item>;
	displayedColumns = ['title', 'franchise', 'type', 'year'];

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;

	constructor(
		private itemService: ItemService,
		public dialog: MatDialog,
		public translate: TranslateService
	) {}

	ngOnInit(): void {
		this.itemService.getItems().subscribe(
			(items: Item[]) => {

				// Sort item by franchise title
				const lang = this.translate.currentLang;
				items.sort(((a, b) => {
					if (a.franchise.title[lang] === b.franchise.title[lang]) {
						return a.year - b.year;
					}
					return a.franchise.title[lang] > b.franchise.title[lang] ? 1 : -1;
				}));

				// Init the table's datasource
				this.dataSource = new MatTableDataSource<Item>(items);
				this.dataSource.paginator = this.paginator;
				this.dataSource.sort = this.sort;

				// Use the title value of the currently set locale
				this.dataSource.sortingDataAccessor = (item, property) => {
					switch (property) {
						case 'title': return item.title[lang];
						case 'franchise': return item.franchise.title[lang];
						default: return item[property];
					}
				};

				// Take any input in any language
				this.dataSource.filterPredicate = function (item, filter: string): boolean {
					// Join together the titles and run includes on the conact string
					const franchiseTitles = Object.values(item.franchise.title).join('').toLowerCase();
					const itemTitles = Object.values(item.title).join('').toLowerCase();
					return itemTitles.includes(filter) || franchiseTitles.includes(filter) || item.year.toString().includes(filter);
				};
			}
		);
	}

	/**
	 * Passes the filter value to the table datasource
	 * @param filterValue
	 */
	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	/**
	 * When clicking on a row, open the Item Dialog
	 * @param item
	 */
	openDetails(item: Item) {
		this.dialog.open(ItemDialogComponent, {
			data: {
				item: item,
				franchises: this.dataSource.data
					.map(it => it.franchise)
			},
			width: '850px'
		});
	}

	/**
	 * Aux function for getting various material icons
	 * @param type
	 */
	getIcon(type: string): string {
		return UtilService.getIcon(type);
	}
}

@Component({
	selector: 'app-item-dialog',
	templateUrl: './item-dialog.component.html',
	styleUrls: ['./item-dialog.component.scss']
})
export class ItemDialogComponent {

	isoLangs = Constants.isoLangs;
	types = Constants.types;
	imageIndex = 0;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data: { item: Item, franchises: Franchise[] },
		public translate: TranslateService
	) {}

	/**
	 * Aux function for getting various material icons
	 * @param type
	 */
	getIcon(type: string): string {
		return UtilService.getIcon(type);
	}

	nextImage(): void {
		this.imageIndex += 1;
		if (this.data.item.releases.length >= this.imageIndex) {
			this.imageIndex = 0;
		}
	}

	previousImage(): void {
		this.imageIndex -= 1;
		if (this.imageIndex < 0) {
			this.imageIndex = this.data.item.releases.length - 1;
		}
	}
}

