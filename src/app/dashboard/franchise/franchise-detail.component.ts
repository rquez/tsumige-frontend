import {Component, OnInit, ViewChild} from '@angular/core';
import {ItemService} from '../../shared/service/item.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {TranslateService} from '@ngx-translate/core';
import {Item} from '../../shared/model/item';
import {UtilService} from '../../shared/service/util.service';
import {Series} from '../../shared/model/series';

@Component({
	selector: 'app-franchise-detail',
	templateUrl: './franchise-detail.component.html',
	styleUrls: ['./franchise-detail.component.scss'],
	providers: [ItemService]
})
export class FranchiseDetailComponent implements OnInit {

	seriesDatasource: MatTableDataSource<Series>;
	seriesColumns = ['title', 'numItems', 'progress'];

	itemDatasource: MatTableDataSource<Item>;
	itemColumns = ['title', 'year', 'type', 'numReleases'];

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

	constructor(
		public translate: TranslateService,
		private itemService: ItemService,
	) {}

	ngOnInit(): void {
		this.itemService.getItems().subscribe(
			(items: Item[]) => {

				// Sort item by franchise title
				items.sort(((a, b) => {
					return a.year - b.year;
				}));

				// Init the table's datasource
				this.itemDatasource = new MatTableDataSource<Item>(items);
				this.itemDatasource.paginator = this.paginator;

				// Use the title value of the currently set locale
				const lang = this.translate.currentLang;
				this.itemDatasource.sortingDataAccessor = (item, property) => {
					if (property === 'title') {
						return item.title[lang];
					} else {
						return item[property];
					}
				};
			}
		);
	}

	/**
	 * Aux function for getting various material icons
	 * @param type
	 */
	getIcon(type: string): string {
		return UtilService.getIcon(type);
	}
}
