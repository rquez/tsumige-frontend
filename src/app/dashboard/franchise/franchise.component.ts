import {Component, OnInit, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {TranslateService} from '@ngx-translate/core';
import {FranchiseMeta} from '../../shared/model/franchise-meta';
import {Router} from '@angular/router';
import {FranchiseService} from '../../shared/service/franchise.service';

@Component({
	selector: 'app-franchise',
	templateUrl: './franchise.component.html',
	styleUrls: ['./franchise.component.scss'],
	providers: [FranchiseService]
})
export class FranchiseComponent implements OnInit {

	dataSource: MatTableDataSource<FranchiseMeta>;
	displayedColumns = ['title', 'numItems', 'lastRelease', 'lastProgress'];

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static: true}) sort: MatSort;

	constructor(
		private franchiseService: FranchiseService,
		public translate: TranslateService,
		private router: Router
	) {}

	ngOnInit(): void {
		this.franchiseService.getFranchiseMetas().subscribe(
			(fms: FranchiseMeta[]) => {
				// Init the table's datasource
				this.dataSource = new MatTableDataSource<FranchiseMeta>(Object.values(fms));
				this.dataSource.paginator = this.paginator;
				this.dataSource.sort = this.sort;

				// Use the title value of the currently set locale
				this.dataSource.sortingDataAccessor = (franchiseMeta, property) => {
					switch (property) {
						case 'title': return franchiseMeta.title[this.translate.currentLang];
						case 'lastItem': return franchiseMeta.lastRelease.title[this.translate.currentLang];
						default: return franchiseMeta[property];
					}
				};

				// Take any input in any language
				this.dataSource.filterPredicate = function (franchiseMeta, filter: string): boolean {
					// Join together the titles and run includes on the conact string
					const franchiseTitles = Object.values(franchiseMeta.title).join('').toLowerCase();
					const lastRelease = Object.values(franchiseMeta.lastRelease.title).join('').toLowerCase();
					return lastRelease.includes(filter) || franchiseTitles.includes(filter);
				};
			}
		);
	}

	/**
	 * Passes the filter value to the table datasource
	 * @param filterValue
	 */
	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	goToFranchise(id: string) {
		this.router.navigateByUrl(`/franchises/${id}`);
	}
}

