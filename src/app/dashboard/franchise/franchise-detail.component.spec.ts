import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {FranchiseDetailComponent} from './franchise-detail.component';

describe('FranchiseDetailComponent', () => {
	let component: FranchiseDetailComponent;
	let fixture: ComponentFixture<FranchiseDetailComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ FranchiseDetailComponent ]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(FranchiseDetailComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
