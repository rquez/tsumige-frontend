import {Component, OnInit, ViewChild} from '@angular/core';
import {FileSystemFileEntry, NgxFileDropEntry,} from 'ngx-file-drop';
import {Papa} from 'ngx-papaparse';
import {ImportRow} from '../../shared/model/import-row';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {ImportService} from '../../shared/service/import.service';
import {from} from 'rxjs';
import {concatMap} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {Item} from '../../shared/model/item';
import {Title} from '../../shared/model/title';
import {Franchise} from '../../shared/model/franchise';
import {Release} from '../../shared/model/release';

@Component({
	selector: 'app-import',
	templateUrl: './import.component.html',
	styleUrls: ['./import.component.scss'],
	providers: [ImportService],
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({height: '0px', minHeight: '0'})),
			state('expanded', style({height: '*'})),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})
export class ImportComponent implements OnInit {

	files: NgxFileDropEntry[] = [];

	importRowDataSource: MatTableDataSource<ImportRow>;
	expandedImportRow: ImportRow | null;

	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

	displayColumns: string[] = ['position', 'iTitle', 'iYear', 'fTitle', 'rTitle', 'status'];

	constructor(
		private papa: Papa,
		private importService: ImportService,
		public translate: TranslateService,
	) { }

	ngOnInit() {
		this.importRowDataSource = new MatTableDataSource<ImportRow>();
		this.importRowDataSource.paginator = this.paginator;
	}

	fileDrop(files: NgxFileDropEntry[]) {

		this.files = files;

		for (const droppedFile of files) {

			// Is it a file?
			if (droppedFile.fileEntry.isFile) {
				const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
				fileEntry.file((file: File) => {

					this.papa.parse(file, {
						header: true,
						dynamicTyping: true,
						skipEmptyLines: true,
						complete: (results) => {

							// Clear database contents
							this.importRowDataSource.data = [];
							const imports = [];

							// Determine languages
							const titleLangs = this.getTitleLangs(results.data);

							// Construct skeleton title object
							const title = titleLangs
								.map(t => ({ [t]: '' }))
								.reduce((accum, t) => ({...accum, ...t}));

							// Go through rows
							results.data.forEach((row, i) => {

								// Parse row
								const importRow = this.parseRow(row, title, i, 'READY');

								// Add to list
								imports.push(importRow);
							});

							// Insert into table
							this.importRowDataSource.data = imports;
						}
					});

				});
			}
		}
	}

	getTitleLangs(rows: any[]) {
		// Get first row
		const columnHeaders = Object.keys(rows[0]);

		// Determine all the languages
		return columnHeaders
			.filter(ch => ch.includes('_title_'))
			.map(t => t.substring(t.length - 2, t.length))
			.filter((l, i, a) => a.indexOf(l) === i)
	}

	parseRow(row: any, title: Title, p, s): ImportRow {

		// Build franchise object
		const franchise: Franchise = {title: {}};
		Object.keys(title).forEach(t => {
			franchise.title[t] = row['franchise_title_' + t];
		});

		// Build release object
		const release: Release = {
			title: {},
			platform: row['platform'],
			progress: row['progress'] || 'NONE',
			players: row['players'],
			stock: row['stock'],
			physical: row['physical'],
			completed: row['done'] || false,
			dateCompleted: row['done_date'],
			dateUpdated: row['last_update'],
			imageLink: row['image'],
			link: {
				link: row['link'],
				source: row['source']
			},
			year: row['release_year']
		};
		Object.keys(title).forEach(t => {
			release.title[t] = row['release_title_' + t];
		});

		// Build item object
		const item: Item = {
			title: { ...title },
			franchise: franchise,
			year: row['item_year'],
			type: row['type'],
			releases: [release]
		};
		Object.keys(title).forEach(t => {
			item.title[t] = row['item_title_' + t];
		});

		return {
			franchise: franchise,
			item: item,
			release: release,
			position: p,
			status: s,
		};
	}

	runImport() {
		from(this.importRowDataSource.data).pipe(
			concatMap(r => {
				r.status = 'UPLOADING';
				return this.importService.importRelease(r);
			})
		).subscribe(
			(r: ImportRow) => {
				// Set new status message
				this.importRowDataSource.data
					.filter(d => d.position === r.position)[0]
					.status = r.status;

				// Move to the next page if needed
				if (r.position === this.paginator.pageSize * (this.paginator.pageIndex + 1)) {
					this.paginator.nextPage();
				}
			},
			() => {
			},
			() => {
			}
		);
	}

}

