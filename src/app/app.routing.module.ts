import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ItemComponent} from './dashboard/item/item.component';
import {ImportComponent} from './dashboard/import/import.component';
import {FranchiseComponent} from './dashboard/franchise/franchise.component';
import {FranchiseDetailComponent} from './dashboard/franchise/franchise-detail.component';

const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: '/items',
	},
	{
		path: '',
		component: DashboardComponent,
		children: [
			{
				path: 'items',
				component: ItemComponent
			},
			{
				path: 'import',
				component: ImportComponent
			},
			{
				path: 'franchises',
				component: FranchiseComponent
			},
			{
				path: 'franchises/:fid',
				component: FranchiseDetailComponent
			},
		]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
	exports: [RouterModule]
})
export class AppRoutingModule {}
