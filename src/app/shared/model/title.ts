export interface Title {
	[lang: string]: string;
}
