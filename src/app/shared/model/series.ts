import {Title} from './title';
import {Franchise} from './franchise';

export interface Series {
	id: string;
	title: Title;
	franchise: Franchise;
}
