import {Title} from './title';

export interface Franchise {
	id?: string;
	title: Title;
}
