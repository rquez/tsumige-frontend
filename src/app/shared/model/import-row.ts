import {Franchise} from './franchise';
import {Item} from './item';
import {Release} from './release';

export interface ImportRow {
	franchise: Franchise;
	item: Item;
	release: Release;

	position: number;
	status: string;
}
