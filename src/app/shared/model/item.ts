import {Franchise} from './franchise';
import {Title} from './title';
import {Release} from './release';

export interface Item {
	id?: string;
	title: Title;
	year: number;
	type: string;
	franchise: Franchise;
	releases: Release[];
}
