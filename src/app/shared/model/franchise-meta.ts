import {Franchise} from './franchise';
import {Item} from './item';
import {Release} from './release';

export interface FranchiseMeta extends Franchise {
	items: Item[];
	lastRelease: Release;
}
