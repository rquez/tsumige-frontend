import {Title} from './title';

export interface Release {
	id?: string;
	title: Title;
	year: number;
	platform: string;
	progress: string;
	players: string;
	stock: string;
	physical: boolean;
	completed: boolean;
	dateCompleted: Date;
	dateUpdated: Date;
	imageLink?: string;
	link: {
		link: string;
		source: string;
	};
}
