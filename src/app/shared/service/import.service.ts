import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {ImportRow} from '../model/import-row';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()
export class ImportService {

	constructor(private http: HttpClient) {}

	importRelease(release: ImportRow): Observable<ImportRow> {
		return this.http.post<ImportRow>('/import', release).pipe(
			catchError((error: HttpErrorResponse) => throwError(error.error))
		);
	}
}
