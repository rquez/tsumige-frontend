import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {FranchiseMeta} from '../model/franchise-meta';
import {Item} from '../model/item';
import {catchError, mergeMap, reduce, shareReplay} from 'rxjs/operators';

@Injectable()
export class FranchiseService {
	private cache$: Observable<FranchiseMeta[]>;

	constructor(private http: HttpClient) {}

	getFranchiseMetas(): Observable<FranchiseMeta[]> {

		if (!this.cache$) {
			this.cache$ = this.http.get<Item[]>(`/item`).pipe(
				mergeMap(fm => fm),
				reduce((accum: FranchiseMeta[], item) => {
					// Get the release with the latest date updated
					const lastRelease = item.releases.reduce((a, b) => {
						return a.dateUpdated > b.dateUpdated ? a : b;
					});
					// Accumulate
					const franchise = accum.find(f => f.id === item.franchise.id);
					if (!franchise) {
						accum.push({
							id: item.franchise.id,
							title: item.franchise.title,
							items: [item],
							lastRelease: lastRelease
						});
					} else {
						franchise.items.push(item);
						franchise.lastRelease =
							lastRelease.dateUpdated > franchise.lastRelease.dateUpdated ?
								lastRelease : franchise.lastRelease;
					}
					return accum;
				}, []),
				shareReplay(1),
				catchError((error: HttpErrorResponse) => throwError(error.error))
			);
		}

		return this.cache$;
	}
}
