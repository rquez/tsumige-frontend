import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, shareReplay} from 'rxjs/operators';
import {Item} from '../model/item';

@Injectable()
export class ItemService {
	private cache$: Observable<Item[]>;

	constructor(private http: HttpClient) {}

	getItems(): Observable<Item[]> {

		if (!this.cache$) {
			this.cache$ = this.http.get<Item[]>(`/item`).pipe(
				shareReplay(1),
				catchError((error: HttpErrorResponse) => throwError(error.error))
			);
		}

		return this.cache$;
	}
}
