export class UtilService {

	static getIcon(type: string): string {
		switch (type) {
			case 'VN':
				return 'art_track';
			case 'BOOK':
				return 'menu_book';
			case 'GAME':
				return 'videogame_asset';
			case 'AUDIO':
				return 'audiotrack';
			case 'PHYSICAL':
				return 'save';
			case 'DIGITAL':
				return 'cloud_download';
			case 'PLAN_BUY':
				return 'shopping_cart';
			case 'OWNED':
				return 'storage';
			case 'PLAYED':
				return 'done';
			case 'STORY':
				return 'check_circle_outline';
			case 'KANSEI':
				return 'check_circle';
		}
	}
}
